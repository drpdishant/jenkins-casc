locals {
  az_count = length(data.aws_availability_zones.available.names) > 1 ? 1 : length(data.aws_availability_zones.available.names)
}
resource "aws_vpc" "main" {
  cidr_block       = var.VpcCIDR
  instance_tenancy = "default"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = var.EnvironmentName
  }
}
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = var.EnvironmentName
  }
}
resource "aws_subnet" "public_subnet" {
count = local.az_count
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.100.${count.index}.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = "main-public-${count.index}"
  }
}

resource "aws_subnet" "private_subnet" {
  count = local.az_count
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.100.${(local.az_count+count.index)}.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = "main-private-${count.index}"
  }
}
resource "aws_eip" "nat_gateway_eip" {
  count = local.az_count
  vpc      = true
}

resource "aws_eip" "instance_ip" {
  vpc      = true
}

resource "aws_nat_gateway" "nat_gw" {
  count = local.az_count
  allocation_id = aws_eip.nat_gateway_eip[count.index].id
  subnet_id     = aws_subnet.public_subnet[count.index].id
  tags = {
    Name = "Nat Gateway ${count.index}"
  }
}
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "${var.EnvironmentName} Public Route"
  }
}
resource "aws_route_table_association" "public_subnet_rtb_assoc" {
  count = local.az_count
  subnet_id      = aws_subnet.public_subnet[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}
resource "aws_route_table" "private_route_table" {
  count = local.az_count
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gw[count.index].id
  }
  tags = {
    Name = "Private Route Subnet ${count.index}"
  }
}

resource "aws_route_table_association" "private_subnet_rtb_assoc" {
  count = local.az_count
  subnet_id      = aws_subnet.private_subnet[count.index].id
  route_table_id = aws_route_table.private_route_table[count.index].id
}
resource "aws_security_group" "internal" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.main.id
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.EnvironmentName} - Internal Security Group"
  }
}

//LoadBalancer For ECS Services
resource "aws_security_group" "lb_sg" {
  name        = "allow_http/s"
  description = "Allow TLS and non TLS inbound traffic"
  vpc_id      = aws_vpc.main.id
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.EnvironmentName} - LoadBalancer SG"
  }
}
