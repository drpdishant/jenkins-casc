output "jenkins_ip" {
 value = aws_eip.instance_ip.public_ip 
}
output "ssh_command" {
 value = "ssh ubuntu@${aws_eip.instance_ip.public_ip}"
}
output "jenkins_url" {
 value = "http://${aws_eip.instance_ip.public_ip}:8080"
}
# output "casc_rendered" {
#     value = data.template_file.casc.rendered
# }
