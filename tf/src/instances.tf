resource "aws_launch_template" "jenkins-slaves" {
  
  name = "jenkins-slave-template"
  image_id = data.aws_ami.ubuntu.id
  instance_initiated_shutdown_behavior = "terminate"
  instance_market_options {
    market_type = "spot"
  }
  vpc_security_group_ids = [aws_security_group.lb_sg.id]
  instance_type = "t3.micro"
  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "jenkins-slave"
      Service = "Jenkins OpenXcell"
    }
  }
  
  key_name = aws_key_pair.deployer.key_name
  user_data = filebase64("../../scripts/setup_slave.sh")
}
resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = file("~/.ssh/id_rsa.pub")
}

/*
resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.jenkins.id
  allocation_id = aws_eip.instance_ip.id
}

resource "aws_instance" "jenkins" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  subnet_id = aws_subnet.public_subnet[0].id
  vpc_security_group_ids = [aws_security_group.lb_sg.id]
  key_name = aws_key_pair.deployer.key_name
  
  tags = {
    Name = "Jenkins"
    Type = "Spot"
  }
  # provisioner "file" {
  #   source      = "../../.docker/plugins.txt"
  #   destination = "/usr/share/jenkins/ref/plugins.txt"

  # }
  # provisioner "file" {
  #   source = data.template_file.casc.rendered
  #   destination = "/var/lib/jenkins/casc.yaml"
  # }
  # user_data = filebase64("../../scripts/setup_master.sh")
  user_data = data.cloudinit_config.master_config.rendered
}
resource "aws_spot_instance_request" "jenkins-slave" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  subnet_id = aws_subnet.private_subnet[0].id
  vpc_security_group_ids = [aws_security_group.lb_sg.id]
  key_name = aws_key_pair.deployer.key_name
  tags = {
    Name = "Jenkins Slave"
    Type = "Spot"
  }
  user_data = filebase64("../../scripts/setup_slave.sh")
}

# Cloud Init Template

locals {
  cloud_config_config = <<-END
    #cloud-config
    ${jsonencode({
      write_files = [
        {
          path        = "/var/lib/jenkins/casc.yaml"
          permissions = "0775"
          owner       = "jenkins:jenkins"
          encoding    = "b64"
          content     = base64encode(data.template_file.casc.rendered)
        },
        {
          path        = "/var/lib/jenkins/plugins.txt"
          permissions = "0775"
          owner       = "jenkins:jenkins"
          encoding    = "b64"
          content     = base64encode(file("../../.docker/plugins.txt"))
        },
      ]
    })}
  END
}
*/
# data "cloudinit_config" "master_config" {
#   gzip          = false
#   base64_encode = false

#   part {
#     content_type = "text/cloud-config"
#     filename     = "cloud-config.yaml"
#     content      = local.cloud_config_config
#   }

#   part {
#     content_type = "text/x-shellscript"
#     filename     = "setup_master.sh"
#     content  = file("../../scripts/setup_master.sh")
#   }
# }

#CASC_TPL

# data "template_file" "casc" {
#   template = file("../templates/casc.tpl")
#   vars = {
#     AGENT_IP = aws_instance.jenkins-slave.private_ip
#     MASTER_IP = aws_eip.instance_ip.public_ip
#     agent_private_key = file("~/.ssh/id_rsa")
#     JENKINS_ADMIN_ID = "admin"
#     JENKINS_ADMIN_PASSWORD = "admin"
#   }
# }
resource "aws_spot_fleet_request" "foo" {
  iam_fleet_role = "arn:aws:iam::473798018071:role/AdministratorAccess"
  spot_price      = "0.005"
  target_capacity = 2

  launch_specification {
    instance_type            = "t3.micro"
    ami                      = data.aws_ami.ubuntu.id
    key_name                 = aws_key_pair.deployer.key_name
    spot_price               = "0.005"
    iam_instance_profile_arn = "arn:aws:iam::473798018071:instance-profile/AdministratorAccess"
    availability_zone        = data.aws_availability_zones.available.names[0]
    subnet_id                = aws_subnet.public_subnet[0].id
  }

}