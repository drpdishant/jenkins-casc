
variable "subnet_count" {
  description = "List of 8-bit numbers of subnets of base_cidr_block that should be granted access."
  default = [1, 2]
}

variable "EnvironmentName" {
    type = string
    default = "testing"
}
variable "VpcCIDR" {
    type = string
    default = "10.100.0.0/16"
}