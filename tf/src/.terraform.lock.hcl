# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "3.21.0"
  hashes = [
    "h1:jvcUKDDNFXjIRWoxfw55vvfFTlSgnLMYe1qiSZNtWoE=",
    "zh:11b8c85d063775029245e86ebce346ed86aa77aefaa72dc558da37ea347aa77c",
    "zh:15f0f5bcbcffe1d2aeaa595f6573f0779932bd3a7647f28ad6aacf1a20f35562",
    "zh:4459e3de50fa9ff64ce50b812ddbcfe7f6fe5fcdd1c23c35a85f11bda5ee8cdb",
    "zh:4a146a958ff5997dca61c016330ed0546093873cce2791cdb727ef897b3d5bdd",
    "zh:698717e66ad6cd58842b696c46c1f4d97c0e3cdad1b872665d79f74e6bbc4310",
    "zh:7e270b37e275d17195993c0b13221e400a67eb26850517977a0587de092a055f",
    "zh:7f14438403ca2ebbe39561f13b78d44bad83d519e907414c2e1f1dc9c2059c0f",
    "zh:9799606edd2079c92e181e8a0f022ac69f2e7093bb23cc9348dc9db9b76aa7da",
    "zh:b947e1a3768650aab0d4679a5202d17464f4ad8c429a57627f9cadf5b78843fc",
    "zh:f7fbd8827afcbab1c5da907283acde543cd0ad8513cec0eea14afb7b926b5a5a",
  ]
}

provider "registry.terraform.io/hashicorp/cloudinit" {
  version = "2.1.0"
  hashes = [
    "h1:L2pM3t/bFVTFpIYq2bBIwt+CeOuwgtfBGaFE1QGHd3c=",
    "zh:169202c899b34b257df4b3cfbf2c5ba45664496cfc3ca1863e554cc3b68048d5",
    "zh:5e7e0f83131bb0a27585fa9118668db7e74f1db9bb43845e1633f2343d002939",
    "zh:6a7a22b292769d9a04d08c5393da5e893befca93769d23aa138ea51c65314da3",
    "zh:70d79043c2dd5473a909241623cde89d5a3688bb98400956d4b577acc81bc2aa",
    "zh:7855ca131f3bb1abbd95d30f7355ec24d5cd0d164c37fb1820466dcad2b51224",
    "zh:7aa8d5d899e870650d94b2fa7b2de918833eda25990676e0bacecd66df0491cf",
    "zh:d8aff5f94edb625f6af532129a07240192e2cd3659052275b3486ef7b7caf55e",
    "zh:e186995d76736e57b008647acbcf1fd992d774b2f19332376a009a8a02297f03",
    "zh:e409f74962e5a4df1862b3a1d74de335c20c6c6f6cc9220afa610c2a77b90749",
    "zh:fd1ef6dd3c9c289f4a3722c336a828457efa108a50b08d97179ee5c531011c1a",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}
