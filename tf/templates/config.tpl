#cloud-config
# vim: syntax=yaml
#
# This is the configuration syntax that the write_files module
# will know how to understand. encoding can be given b64 or gzip or (gz+b64).
# The content will be decoded accordingly and then written to the path that is
# provided. 
#
# Note: Content strings here are truncated for example purposes.
cloud_final_modules:
- [scripts-user, always]

write_files:
- encoding: b64
  content: ${casc_content}
  owner: jenkins:jenkins
  path: /var/lib/jenkins/casc.yaml
  permissions: '0644'