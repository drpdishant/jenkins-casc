jenkins:
  nodes:
  - permanent:
      launcher:
        ssh:
          credentialsId: "agent-private-key"
          host: "${AGENT_IP}"
          port: 22
          sshHostKeyVerificationStrategy: "nonVerifyingKeyVerificationStrategy"
      name: "jenkins-slave"
      remoteFS: "/tmp"
      retentionStrategy: "always"
  remotingSecurity:
    enabled: true
  securityRealm:
    local:
      allowsSignup: false
      users:
       - id: ${JENKINS_ADMIN_ID}
         password: ${JENKINS_ADMIN_PASSWORD}
  authorizationStrategy:
    globalMatrix:
      permissions:
        - "Overall/Administer:admin"
        - "Overall/Read:authenticated"
security:
  queueItemAuthenticator:
    authenticators:
    - global:
        strategy: triggeringUsersAuthorizationStrategy
unclassified:
  location:
    url: "http://${MASTER_IP}:8080"

credentials:
  system:
    domainCredentials:
    - credentials:
        - basicSSHUserPrivateKey:
            scope: SYSTEM
            id: agent-private-key
            username: ubuntu
            passphrase:  ""
            description: "ssh private key used to connect ssh slaves"
            privateKeySource:
              directEntry:
                privateKey: ${agent_private_key}