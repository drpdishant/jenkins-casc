export CHROME_DRIVER_VERSION="88.0.4324.96" \
export CHROME_DRIVER_BASE="chromedriver.storage.googleapis.com" \
export CPU_ARCH="64"
export CHROME_DRIVER_FILE="chromedriver_linux${CPU_ARCH}.zip"
export CHROME_DRIVER_URL="https://${CHROME_DRIVER_BASE}/${CHROME_DRIVER_VERSION}/${CHROME_DRIVER_FILE}"

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub \
    | apt-key add - \
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" \
    > /etc/apt/sources.list.d/google.list
apt-get -qy update
apt-get -qy install -y google-chrome-stable 
apt-get -qyy autoremove
rm -rf /var/lib/apt/lists/* 
apt-get -qyy clean 

apt-get update; apt-get install -y unzip
wget -nv -O chromedriver_linux${CPU_ARCH}.zip ${CHROME_DRIVER_URL}
unzip chromedriver_linux${CPU_ARCH}.zip
rm chromedriver_linux${CPU_ARCH}.zip
chmod +x chromedriver; install chromedriver /usr/local/bin

apt-get -y install default-jdk-headless maven
curl -sSL https://get.docker.com/ | bash -