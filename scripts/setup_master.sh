#!/bin/bash
cat >> /etc/environment << EOF
JAVA_OPTS="-Djenkins.install.runSetupWizard=false"
CASC_JENKINS_CONFIG="/var/lib/jenkins/casc.yaml"
JENKINS_PLUGINS_LIST="/usr/share/jenkins/ref/plugins.txt"
EOF
cat >> /bin/jenkins-plugin-cli << EOF
#!/bin/bash
exec /bin/bash -c "java $JAVA_OPTS -jar /usr/lib/jenkins-plugin-manager.jar $*"
EOF
chmod +x /bin/jenkins-plugin-cli
curl -Lo /usr/lib/jenkins-plugin-manager.jar https://github.com/jenkinsci/plugin-installation-manager-tool/releases/download/2.5.0/jenkins-plugin-manager-2.5.0.jar 



wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
set -x
apt-get -qq update
apt-get -qq install default-jdk -y 
sleep 10
apt-get -qq install jenkins -y 
echo "Installing Jenkins Plugins"
jenkins-plugin-cli -f $JENKINS_PLUGINS_LIST
systemctl start jenkins
systemctl status jenkins
ufw disable
set +x 