# Jenkins Setup with Configuration as Code Plugin.

## Overview

- This project consists of Containerized Jenkins built with a Dockerfile and docker-compose stack for unattented setup and configuration with JCasC Plugin.
- This also demonstrates unattented configuration of Jenkins Slaves using JCasC.
- The primary aim achieved here is automated setup and configuration of Jenkins which can be easily replicated in cloud native environments, such as AWS EC2, Google Compute and Kubernetes. This also helps demonstrate dynamic configuration of jenkins slaves nodes, which can be used to configure slaves with autoscaling groups, spot fleet and kubernetes executors.

Following is the list of required files and their description:

| File Path | Description |
| --- | --- |
| [`.docker/Dockerfile`](.docker/Dockerfile) | Dockerfile to build the custom Jenkins Image which preinstalls the plugins from plugins.txt |
| [`.docker/plugins.txt`](.docker/plugins.txt) | List of Plugins to be installed while building the docker image. Although its not necessary to define all the plugins required, those ones to be used for initial configuration must be there |
| [`.docker/casc.yaml`](.docker/casc.yaml) | This is the config file used by JCasC to initialize the Jenkins Configuration | 
| [`docker-compose.yml`](docker-compose.yml) | Docker Compose file to start up the jenkins server |

## Setup
### Vagrant Up (Slave Machines)

Vagrant machine is provisioned with JDK, Chrome Driver and Chrome Browser.

```bash
vagrant up --provision
```

It will be locally accesible on host at `192.168.33.10`
Jenkins will use this machine as Slave Node as configured in casc.yaml to run the jobs.

### Build 

Its is required to build the Jenkins Image when:
- You are doing a fresh setup
- You have modified the plugins.txt

```bash
docker-compose build
```

### Start

```bash
docker-compose up -d
```

OR if you want to force recreate the container, to ensure volumes are remounted.

```bash
docker-compose up -d --force-recreate
```

> SSH for the slave node is automatically added using secret parameter in docker-compose.

### Customizing 

#### Plugins:

- To Customize the plugins you can add your plugin name, and version if required to [plugins.txt](.docker/plugins.txt)
- To get the corret plugin name and version browse the plugins repository at https://plugins.jenkins.io. Choose your plugin, and go to Releases tab.
Here you'll get the full command for jenkins plugins cli with exact plugin name and latest version.

![Plugin Registry](./plugin-registry.png)

### CASC

- Customizations in CASC don't require building the docker image, but if you are applying any plugin specific configuration, e.g add SSH Slave Nodes, then ensure that you add the plugin to list and build a new image.